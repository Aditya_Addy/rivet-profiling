#!/bin/bash

RBD=/home/addy/Desktop/gsoc2020/rivet/dev_rivet/local

g++ -g -O3 -std=c++14  rivet_app.cxx  -o rivetapp \
    -I${RBD}/include \
    -L${RBD}/lib64 -lRivet \
    -L${RBD}/lib64 -lHepMC \

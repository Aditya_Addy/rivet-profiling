# Rivet Profiling

**Before Running make sure Rivet is installed and change the RBD in build.sh to your rivet build directory**

## Methodology

This program reads from the event file `event_100.hepmc` which consist of 100 event,and stores in the array.

Now we iterate for 10,000 times and in each iteration we randomly selects one event from the array and sends it to the `rivet analysis handler` to analyze it.


# Intel VTune Profiling

## Hotspot Analysis
---
It identifies the most time consuming functions.

![Hotspot Analysis Summary](./vtune/images/1_hotspot_summary.png)

**The Top Hostspots are not in rivet but are in HepMC**


![Hotspot Analysis Top Down](./vtune/images/2_hotspot.png)


**Top Down tree will further help in tracking down the hostspots down to the each line in the rivet**

## Microarchitecture Analysis
---
This is hardware event based sampling collection which analyzes CPU microarchitecture bottlenecks affecting the application 

![Microarchitecture analysis Summary](./vtune/images/3_ME.png)

**Overall CPI(Cycles per Instruction Retired) is 0.762 which is good considering rivet contains mostly instruction bound code. Value around 0.25 is ideal and various optimization which could improve floating point operation and branch misprediction can help us in lowering down the value** 

![Microarchitecture analysis Summary](./vtune/images/4_ME.png)

**Further the Bottom up table(Shows pipeline execution by group) could be used to find out various factor affecting the CPI**


## HPC Performance Characterization
---
It analyzes CPU utilization, Memory usage and FPU utilization with vectorization information

![HPC](./vtune/images/5_HPC.png)

**Result is mostly influnced by HepMC but there could be scope of improvement in vectorization**



# Cache Grind

Cachegrind simulates how programs interacts with a machine's cache hierarchy and(optionally) branch predictor. It simulates a machine with independent first-level instruction and data caches (I1 and D1), backed by a unified second-level cache (L2)

Output can be found in cacheGrind directory, though the L1(I1 as well as D1) Cache miss percent isn't significant.

# Gprof

It collects and arranges statistics of the program. The output can be found in gprof directory. The output is not very diffrent than that of the vtune, infact it is vary small subset of vtune hotspot analysis.


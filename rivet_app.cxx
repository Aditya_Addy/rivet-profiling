#include "Rivet/AnalysisHandler.hh"

#include "HepMC/IO_GenEvent.h"

#include <cstdlib>
#include <ctime>
#include <random>

using namespace std;

// int uniform_random_number(){
//     std::random_device rand_dev;
//     std::mt19937 generator(rand_dev());
//     std::uniform_int_distribution<int>  distr(0, 99);
//     return distr(generator);
//}

void run()
{
    Rivet::Log::setLevel("Rivet", Rivet::Log::ERROR);
    Rivet::AnalysisHandler *ah = new Rivet::AnalysisHandler();


    ah->addAnalysis("MC_TTBAR");
    ah->addAnalysis("MC_JETS");

    std::ifstream istr("event_100.hepmc");
    HepMC::IO_GenEvent ascii_in(istr);
    HepMC::GenEvent *evt = ascii_in.read_next_event();
    HepMC::GenEvent *evt1[105];

    int icount = 0;
    while (evt)
    {
        evt1[icount] = evt;
        icount++;
        //delete evt;
        ascii_in >> evt;
    }
    for (int i = 0; i < 10000; i++)
    {
        int randomNumber = (rand() % 100); //random_number();
        try
        {
            ah->analyze(*evt1[randomNumber]);
        }
        catch (const std::exception &e)
        {
            cerr << "Exception in Rivet::analyze: " << e.what() << "\n";
        }
    }

    //ah->setCrossSection(1.0E9, 1e9);
    ah->finalize();
    ah->writeData("output.yoda");
    cout << "done\n";
    return;
}

int main()
{
    srand((unsigned)time(0));
    run();
    return 0;
}
